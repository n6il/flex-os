; Flex system configuration
;
; Uncomment lines to enable options

;CON32       equ 1     ;32x16 Text display (using 512 byte text mode)
CON51       equ 1     ;51x24 Text display (using 6KB graphics mode)

BECKER      equ 1     ; Build for Becker port (instead of serial)
RAMDRIVE    equ 1     ; Create ram drive (requires memory expansion)

; Select one memory expansion option only
MOOH        equ 1       ; Build for MOOH
;BANK256     equ 1       ; Build for 256K banker board



; General defs

MAXDRIVENUM equ 3     ; Maximum drive number 0-3
RAMDRIVENUM equ MAXDRIVENUM
