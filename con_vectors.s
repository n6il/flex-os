; console i/o vector table
;
; this is assembled separately as it is not contiguous
; with the console or disk driver code
; $d370 - $d3e4 is free however

				include "console.inc"

				org $d3e1

; reserve somewhere for flex to store vectors
; (flex writes to these locations on startup)
_swivec	rmb 2
_irqvec rmb 2

				org $d3e5

INCHNE  fdb con_waitkey  ; input character no echo
IHNDLR  fdb con_irq_h    ; irq interrupt handler
SWIVEC  fdb _swivec      ; swi3 vector location
IRQVEC  fdb _irqvec      ; irq vector location
TMOFF   fdb con_tmr_off  ; timer off routine
TMON    fdb con_tmr_on   ; timer on routine
TMINT   fdb con_tmr_init ; timer initialisation
MONITR  fdb $cd03        ; monitor entry address
TINIT   fdb con_init     ; terminal initialisation
STAT    fdb con_status   ; check terminal status
OUTCH   fdb con_outch    ; output character
INCH    fdb con_echokey  ; input character with echo
