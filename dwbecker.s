; Drivewire read/write functions for Becker port
; S.Orchard Nov 2021
;
; Entry & exit conditions compatible with
; Darren Atkinson's bitbang serial code
;

;BECKER_STS  equ $ff41  ; (rsdos)
BECKER_STS  equ $ff49  ; (dgndos)
BECKER_DATA  equ BECKER_STS + 1

; read from drivewire server
;   x = buffer
;   y = count
; returns:
;   x = buffer
;   y = checksum
;   z set on success
;   carry clear
;   u preserved

DWRead
    clrb              ; init timeout lsb
    pshs b,x,u
    leau ,x           ; buffer address
    ldx #0            ; zero checksum
    lda #2            ; position of Becker port status bit
1   clr ,s            ; init timeout msb & clear carry
2   bita BECKER_STS
    bne 5f            ; data available
4   decb
    bne 2b
    dec ,s            ; timeout msb
    bne 2b
    incb              ; clear z
    puls a,x,u,pc     ; error exit
5   ldb BECKER_DATA
    abx               ; update checksum
    stb ,u+
    leay -1,y
    bne 1b
    leay ,x           ; return checksum in y
    clrb              ; set z
    puls a,x,u,pc     ; successful exit


; write to drivewire server
;   x = buffer
;   y = count
; returns:
;   x = buffer + count
;   y = 0
;   other regs preserved

DWWrite
    pshs cc,a
1   lda ,x+
    sta BECKER_DATA
    leay -1,y
    bne 1b
    puls cc,a,pc
