; Initialise ram disk for Flex
; S.Orchard Nov 2021

  IF RAMDRIVE

NUMTRACKS  equ 48   ; 48 x 8 x 256 = 96K
NUMSECTORS equ 8    ;

; Flex routines
WARMS   equ $cd03   ; warm start
PUTCHR  equ $cd18   ; output char
PSTRNG  equ $cd1e   ; output string
PCRLF   equ $cd24   ; output CRLF
OUT2HS  equ $cd3c   ; output 2 hex digits

DREAD   equ $de00   ; read sector
DWRITE  equ $de03   ; write sector
DRIVE   equ $de0c   ; select drive



      ; standard flex utility entry
      org $c100

      bra start

version fcb 1

error
      pshs b
      ldx #errmsg
      jsr PSTRNG
      leax ,s
      jsr OUT2HS
      puls b
      jsr PCRLF
      jmp WARMS

start
      orcc #$50

      ldx #msg1
      jsr PSTRNG

      jsr init_banks

      ; select drive
      ldx #drivenum-3   ; fake fcb with drivenum
      jsr DRIVE
      bne error

      ; set up system information record
      jsr clr_secbuf
      ldx #secbuf+16
      ldu #sirinfo
      ldb #sirinfosize
1     lda ,u+
      sta ,x+
      decb
      bne 1b

      ldx #secbuf
      ldd #$0003
      jsr DWRITE
      bne error

      ; read it back so that disk routines update geometry
      ldx #secbuf
      ldd #$0003
      jsr DREAD
      bne error


      ; write link field to every sector

      jsr clr_secbuf
      ldd #$0004
      std prevtrack

      clr track
      lda #5
      bra secloop
initsec
      lda #1
secloop
      sta sector

      ldu track
      ldd prevtrack
      stu prevtrack
      bsr init_sector_link

      lda #'.
      jsr PUTCHR

      ; next sector
      lda sector
      inca
      cmpa #NUMSECTORS
      bls secloop
      inc track
      lda track
      cmpa #NUMTRACKS
      blo initsec

      ; final sector
      ldd prevtrack
      ldu #0
      bsr init_sector_link

      ; terminate directory track
      ldd #NUMSECTORS
      ldu #0
      bsr init_sector_link

      ldx #okmsg
      jsr PSTRNG

      jmp WARMS


; location in D, forward link in U
init_sector_link
      ldx #secbuf
      stu ,x
      jsr DWRITE
      lbne error
      rts


clr_secbuf
      ldx #secbuf
      clra
1     clr ,x+
      deca
      bne 1b
      rts



init_banks

 IF MOOH
      ; mooh has 64 x 8K pages of ram
      ; page $3f generally selects Dragon internal memory
      ; except in range $e000-feff, where $3f is actual mooh ram

      ldd #$3f3f    ; init memory map
      std $ffa0
      std $ffa2
      std $ffa4
      std $ffa6
      clr $ff91     ; set task 0

      ; copy $e000-feff to mooh page $3f
      ; (mooh puts own ram here when enabled)
      ldx #$e000
      ldd #$0040
1     sta $ff90
      ldu ,x
      stb $ff90
      stu ,x++
      cmpx #$ff00
      blo 1b

      ; copy $0000-$1fff to mooh page $00
      ; (Dragon internal ram always gets written so we need to put
      ;  mooh ram wherever we intend to use paging)
    	ldx #0
      ldd #$3f00
1     sta $ffa0
      ldu ,x
      stb $ffa0
      stu ,x++
      cmpx #$1fff
      blo 1b

 ELSIF BANK256
      ; 256K banker board has 4 x 32K lower banks
      ; and 4 x 32K upper banks

      sta $ff39   ; keep video in bank 0
 ENDIF

      rts



msg1    fcb /INITIALISING RAMDISK.../,4
errmsg  fcb 13,10,/ERROR /,4
okmsg   fcb 13,10,/COMPLETE/,13,10,4

sirinfo   fcc /RAMDISK/,0,0,0,0       ; volume name
          fdb 1                       ; volume number
          fcb 1,1                     ; data start
          fcb NUMTRACKS-1,NUMSECTORS  ; data end
          fdb (NUMTRACKS-1)*NUMSECTORS  ; number of data sectors
          fcb 1,1,21                  ; date
          fcb NUMTRACKS-1             ; max track number
          fcb NUMSECTORS              ; max sector number
sirinfosize equ *-sirinfo


drivenum  fcb RAMDRIVENUM
track     rmb 1
sector    rmb 1
prevtrack rmb 2

secbuf    rmb 256

  ENDIF   ; IF RAMDISK
