; Dragon console driver for FLEX
;
; 32 x 16 Text Screen
;
; S.Orchard October 2021


CON_VIDEND    equ $fdff
CON_VIDBASE   equ CON_VIDEND - $1ff

VID_SPACE     equ #$20    ; video code for space character
VID_CURSOR    equ #$ef    ; video code for cursor

; ascii to 6847 mapping
;   20-3f -> 20-3f
;   40-5f -> 00-1f
;   60-7f -> 40-5f (inverse)


        section "code"


; wait for key press with flashing cursor
; returns key in A
con_waitkey
        bsr show_cursor
2       bsr blink_cursor
        jsr  kbd_scan
        beq  2b
        bra remove_cursor


show_cursor
        pshs a,x
        clr cursor_visible
        bra 1f

blink_cursor
        pshs a,x
        ldx cursor_count
        leax -1,x
        bne 9f
1       ldx #750
        lda #VID_CURSOR
        com cursor_visible
        bne 8f
5       lda #VID_SPACE
8       sta [con_vidpos]
9       stx cursor_count
        puls a,x,pc

remove_cursor
        pshs a,x
        clr cursor_visible
        ldx #1
        bra 5b


; wait for key and echo to screen
; returns key in A

con_echokey
        bsr con_waitkey
con_outch
        pshs d,x
        ldx con_vidpos
        anda #$7f
        cmpa #$20
        blo 3f
        cmpa #$40
        blo 5f
        cmpa #$60
        blo 4f
        eora #$20
        bra 5f

3       cmpa #13
        beq con_cr
        cmpa #08
        beq con_bs
        cmpa #09
        beq con_tab
        cmpa #10
        beq con_lf
        cmpa #12
        bne 9f
        jmp con_ff

4       eora #$40
5       sta ,x+
6       cmpx #CON_VIDEND
        bhi con_scroll
8       stx con_vidpos
9       puls d,x,pc

con_cr
        tfr x,d
        andb #$e0
        tfr d,x
        bra 8b

con_lf
        leax 32,x
        bra 6b

con_bs
        cmpx #CON_VIDBASE
        bls 9b
        lda #VID_SPACE
        sta ,-x
        bra 8b

con_tab
        tfr x,d
        andb #$f8
        tfr d,x
        leax 8,x
        bra 6b

con_scroll
        pshs u
        leax -32,x
        stx con_vidpos
        ldx #CON_VIDBASE
        ldb #240
1       ldu 32,x
        stu ,x++
        decb
        bne 1b
        ldd #$2020
1       std ,x++
        cmpx #CON_VIDEND
        blo 1b
5       puls u
        puls d,x,pc


con_ff
        pshs u
        ldx #CON_VIDBASE
        stx con_vidpos
        clrb
        ldu #VID_SPACE*257
1       stu ,x++
        decb
        bne 1b
        bra 5b


con_init2
        clr cursor_visible
        ldd #1
        std cursor_count
        rts


        section "convars"

con_vidpos      rmb 2
cursor_visible  rmb 1
cursor_count    rmb 2
