; Dragon DriveWire disk i/o for FLEX
;
; S.Orchard October 2021

    section "code"

; read sector
; x = buffer
; a = track
; b = sector
; returns z = 1 if no error
; b = error condition (b7=not ready, b4=not found, b3=crc error, b2=lost data)
;
; if system info record sector is read, updates sectors per track

dk_read
    pshs y,u
    jsr dk_setupdrivelsn
    pshs x
  IF RAMDRIVE
    cmpa #RAMDRIVENUM
    beq _dk_ram_read
  ENDIF
    jsr dw_read_sec
_dk_read
    puls x
    lbne dk_error
    ldy #dw_buf
    lda #128
1   ldu ,y++
    stu ,x++
    deca
    bne 1b
    ldd dk_track_no
    subd #$0003     ; system info record?
    bne 2f
    lda dw_buf+39
    sta [dk_p_sectors_per_trk]
2   clrb        ; no error
    puls y,u,pc


  IF RAMDRIVE

_dk_ram_read
    bsr _ram_map_in

    ldy #dw_buf
    lda #128
1   ldu ,x++
    stu ,y++
    deca
    bne 1b

    bsr _ram_map_out
    clrb          ; no error
    bra _dk_read


_ram_map_in
    ldd dw_lsnm   ; determine 32K bank number
    addd #$80     ; skip first 32K
    cmpa #2       ; ensure within range
    blo 1f        ;
    ldd #$1ff     ; limit to max value
1
    lslb          ;
    rola          ;

  IF MOOH
    lslb          ;
    rola          ;
    lslb          ;
    rola          ;
    ldu #$ffa0    ; setup mooh
    sta ,u
    lsrb          ; offset into 8K bank
    lsrb
    lsrb
    tfr b,a
    clrb
    tfr d,x
  ELSE
    ldu #$ff30    ; setup 256K banker
    sta a,u
    lsrb          ; offset into 32K bank
    tfr b,a       ;
    clrb          ;
    tfr d,x       ;
  ENDIF

    rts

_ram_map_out
  IF MOOH
    clr $ffa0     ; restore lower 8K
  ELSE
    sta $ff30     ; restore lower 32K
  ENDIF
    rts

  ENDIF    ; RAMDRIVE

; write sector
; x = buffer
; a = track
; b = sector
; returns z = 1 if no error
; b = error condition (b7=not ready, b6=write protect, b4=not found, b3=crc error, b2=lost data)

dk_write
    pshs y,u
    stx dk_writeaddr
    bsr dk_setupdrivelsn
    ldy #dw_buf
    lda #128
1   ldu ,x++
    stu ,y++
    deca
    bne 1b
  IF RAMDRIVE
    lda dk_drive_no
    cmpa #RAMDRIVENUM
    beq _dk_ram_write
  ENDIF
    jsr dw_write_sec
    bne dk_error
    puls y,u,pc


  IF RAMDRIVE

_dk_ram_write
    bsr _ram_map_in

    ldy #dw_buf
    lda #128
1   ldu ,y++
    stu ,x++
    deca
    bne 1b

    bsr _ram_map_out
    clrb          ; no error
    puls y,u,pc

  ENDIF

; convert drivewire error into WD status
dk_error
    tfr b,a
    ldb #$40
    cmpa #242   ; write protect
    beq 1f
    ldb #$08
    cmpa #243   ; checksum
    beq 1f
    ldb #$80    ; not ready
1   coma        ; set carry
    tstb        ; clear z
    puls y,u,pc

; set up drive and logical sector number
; a = track
; b = sector
; returns:
;   a = drive num
dk_setupdrivelsn
    std dk_track_no   ; track and sector
    ldb [dk_p_sectors_per_trk]
    mul
    addb dk_sector
    adca #0
    subd #1
    clr dw_lsnh
    std dw_lsnm
    lda dk_drive_no
    sta dw_drv
    rts


; verify sector just written
; returns z = 1 if no error
; b = error condition (b7=not ready, b4=not found, b3=crc error, b2=lost data)

dk_verify
    pshs y,u
  IF RAMDRIVE
    lda dk_drive_no   ; skip verify if ram disk
    cmpa #RAMDRIVENUM ;
    beq 5f            ;
  ENDIF
    jsr dw_read_sec
    bne dk_error
    ldu dk_writeaddr
    ldy #dw_buf
    ldd #$8008    ; a=128, b=crc error code
1   ldx ,y++
    cmpx ,u++
    bne 9f
    deca
    bne 1b
5   clrb        ; no error
9   puls y,u,pc

; select and restore drive to track 0
; x = fcb (drive number at 3,x)
; returns z = 1 if no error
; b = error condition (b7=not ready, b4=seek error, b3=crc error)

dk_restore
    bsr dk_select
    bne 9f
    clr dk_track_no
    clrb            ; set z, no error
9   rts

; select drive
; x = fcb (drive number at 3,x)
; returns z = 1, c = 0 if no error
; b = $0f for non-existent drive, $80 for not ready

dk_select
    lda 3,x
    cmpa #MAXDRIVENUM
    bhi 9f          ; error
    sta dk_drive_no
    ldx #dk_drive_table
    leax a,x
    stx dk_p_sectors_per_trk
    clrb            ; clear c, set z, no error
    rts
9   comb            ; set carry
    ldb #$0f        ; error = non-existent drive
    rts


; check drive ready
; x = fcb (drive number at 3,x)
; returns z = 1, c = 0 if no error
; b = error condition (b7=not ready)

dk_check

; quick check drive ready
; x = fcb (drive number at 3,x)
; returns z = 1, c = 0 if no error
; b = error condition (b7=not ready)

dk_qcheck
    lda 3,x     ; drive number
    cmpa #MAXDRIVENUM
    bhi 9f          ; error
    clrb        ; set z, clear c, no error
    rts
9   comb        ; set carry
    ldb #$0f    ; error = non-existent drive
    rts


; cold start initialisation
dk_init
    clra
    clrb
    sta dk_drive_no
    sta dk_track_no
    sta dk_sector
    ldx #dk_drive_table
    stx dk_p_sectors_per_trk
    std ,x
    std 2,x

; warm start initialisation
dk_warm
    rts


; seek track
; a = track
; b = sector (indicates side)
; returns z = 1 if no error
; b = error condition (b7=not ready, b4=seek error, b3=crc error)

dk_seek
    sta dk_track_no
    clrb            ; set z, no error
    rts


    section "vars"

; disk i/o variables
dk_drive_no           rmb 1
dk_track_no           rmb 1
dk_sector             rmb 1
dk_writeaddr          rmb 2   ; pointer to last data written
dk_drive_table        rmb 4   ; sectors per track for each drive
dk_p_sectors_per_trk  rmb 2   ; pointer to current sectors per track


; -----------------------------------------------
; drivewire layer

; dw server error codes
;DW_ERR_WP = 242
;DW_ERR_CHECKSUM = 243
;DW_ERR_READ = 244
;DW_ERR_WRITE = 245
;DW_ERR_NOTREADY = 246


    section "code"

; send 0xd2,drive,lsnh,lsnm,lsnl
; read sector data
; send chkhi,chklo
; read error code

dw_read_sec
    ldx #dw_cmd
    lda #$d2          ; readex opcode
    sta ,x            ;
    ldy #5            ;
    jsr DWWrite       ; send readex command
    ldy #256          ;
    bsr DWRead        ; read sector
    bne dw_read_error
    ldx #dw_chk
    sty ,x            ; checksum
    ldy #2
    jsr DWWrite       ; send checksum
    ldy #1
    bsr DWRead        ; read status byte
    bne dw_read_error
    ldb ,x            ; return status in B & Z
    rts

dw_read_error
    ldb #246          ; not ready
    rts

; send 0x57,drive,lsnh,lsnm,lsnl,sector,chkhi,chklo
; read error code

dw_write_sec
    bsr dw_checksum
    ldx #dw_cmd
    lda #$57          ; write sector opcode
    sta ,x            ;
    ldy #263          ;
    jsr DWWrite       ; send write sector command
    ldy #1
    bsr DWRead        ; read status byte
    bne dw_read_error
    ldb ,x            ; return status in B & Z
    rts


; determine checksum
; 16 bit sum of sector bytes
dw_checksum
    ldu #dw_buf
    ldx #0
    clra
1   ldb ,u+
    abx
    deca
    bne 1b
    stx dw_chk
    rts

 IF BECKER
    include "dwbecker.s"
 ELSE
    include "dwserial.s"
    setdp -1    ; dwserial uses dp
 ENDIF

    section "vars"

; drivewire command & sector buffer
; order of fields required for routines

dw_cmd       rmb 1
dw_drv       rmb 1
dw_lsnh      rmb 1
dw_lsnm      rmb 1
dw_lsnl      rmb 1
dw_buf       rmb 256
dw_chk       rmb 2
dw_err       rmb 1
