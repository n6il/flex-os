
        section "code"

; check for key press
; returns with z=1 if no key available

con_status
        pshs a
        clra
        sta $ff02
        lda $ff00
        anda #$7f
        cmpa #$7f
        beq 1f
        jsr kbd_scan
1       puls a,pc


; set up hardware

con_init
        ldx #$ff00    ; disable interrupts & clear flags
        lda #$34
        sta 1,x
        sta 3,x
        sta $21,x
        lda ,x
        lda 2,x
        leax $20,x
        lda #$36
        sta 3,x
        lda ,x
        lda 2,x

        jsr con_init2      ; console driver specific setup
        jsr kbd_init

        ; setup SAM video base
        ldx #$ffc6
        lda #CON_VIDBASE >> 9
1       clrb
        lsra
        rolb
        sta b,x
        leax 2,x
        cmpx #$ffd4
        blo 1b

        lda #12          ; clear screen
        jmp con_outch


con_irq_h
        lda $ff02
        rti

con_tmr_off
con_tmr_on
con_tmr_init
        rts


        include "keyboard.s"

    if CON32
        include "con32.s"
    elsif CON51
        include "con51.s"
    endif


        ; export symbols required to build the console vector table

        export con_status, con_waitkey
        export con_init, con_outch, con_echokey
        export con_irq_h
        export con_tmr_off, con_tmr_on, con_tmr_init
