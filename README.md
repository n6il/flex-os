## FLEX OS adapted for the Dragon 64 and DriveWire

FLEX is a disk operating system originally developed in 1976 for the Motorola 6800 processor by Technical Systems Consultants, and then ported to the Motorola 6809 in 1979.

This project takes the original 1979 core of FLEX and adds drivers for console i/o and DriveWire disks.

The system can be built either for DriveWire over serial or Becker port allowing testing in an emulator.

### Motivation

While it's true that Flex has already been ported to the Dragon by Compusense and similarly for the CoCo by Frank Hogg Labs, documentation covering the details of these adaptations appears to be hard to find.

By writing the drivers from scratch and making the source code available, we have a much clearer picture of how the system works and can be confident that the original TSC documentation is an accurate description of the system.

### Implementation Notes

Flex requires at least 64K of memory on a Dragon.

The `DATE` command has been modified to be Y2K compliant. A threshold value of 75 has been chosen to give the range 1975-2074.

`XBASIC` has been patched to respond to keyboard break.

Type in the command `VERIFY OFF` to make disk writes faster.

The text display may be chosen as either 32x16 or 51x24.

A ram disk option is available and uses either Tormod Volden's MOOH cart (only tested in an emulator), or my 256K banker board. The `INITRAMD` command should be run before using the ram drive. The ram drive number is defined to be drive 3.

The Dragon/CoCo memory map is not fully compliant with the requirements to run Flex, due to the presence of the interrupt vectors low down in user ram. To work round this, all interrupts have been disabled at the PIA level, which means the printer spool feature is not available.

If an application requires interrupts, it will need to specifically set up the vectors and enable the required interrupts, disabling them again on exit. I imagine that the existing adaptations did something along these lines.

### Building

Requires [asm6809](https://www.6809.org.uk/asm6809/) to assemble the source files.

Edit `config.s` to select build options.

A Makefile is included for those systems that support it (so just `$ make`) otherwise you will need to develop your own build script based on the following steps:

First build and convert the drivers to Flex binary format:
```
asm6809 -D -o drivers.bin -E console.inc config.s drivers.s
asm6809 -D -o con_vectors.bin config.s con_vectors.s
tools/ddbin2flex.py drivers.bin drv.sys --notransfer
tools/ddbin2flex.py con_vectors.bin convec.sys --transfer 0xcd00
```
Then append the drivers to the Flex core: (e.g. in Unix)
```
cat flex.cor drv.sys convec.sys > flex.sys
```
or in Windows:
```
copy /b flex.cor + drv.sys + convec.sys flex.sys
```
For the bootloader:
```
asm6809 -D -o FLEXLOAD.BIN config.s flexload.s
```

To build a disk image containing commands:
```
cd disks
asm6809 -D -o DATE.BIN DATE.TXT
asm6809 -D -o INITRAMD.BIN ../config.s INITRAMD.TXT
../tools/ddbin2flex.py DATE.BIN DATE.CMD
../tools/ddbin2flex.py INITRAMD.BIN INITRAMD.CMD
../tools/ddbin2flex.py XBASIC.BIN XBASIC.CMD
../tools/flexdsktool.py flex.dsk --newdisk --addfile ERRORS.SYS --random
../tools/flexdsktool.py flex.dsk --addfile DATE.CMD
../tools/flexdsktool.py flex.dsk --addfile INITRAMD.CMD --zerolencheck
../tools/flexdsktool.py flex.dsk --addfile XBASIC.CMD
../tools/flexdsktool.py flex.dsk --addfile flexfiles/APPEND.CMD
```
(Last step is repeated for each file to be added from the `flexfiles` folder)

To build a blank disk image:
```
../tools/flexdsktool.py test.dsk --newdisk
```

### Boot method

First load the Dragon DOS binary `FLEXLOAD.BIN` either via DriveWire, or directly into an emulator.

When run, this will load the Flex image file `flex.sys` into high memory and get Flex running.

### Useful links

[Flex User Group](http://www.flexusergroup.com/flexusergroup/default.htm) (good source of documentation)

Lots of FLEX disk images [here](http://www.evenson-consulting.com/swtpc/fufu_downloads.htm)

My own minimal [DriveWire server](https://gitlab.com/sorchard001/dwlserve)

### Original FLEX copyright

Copyright @ 1979 by
Technical Systems Consultants Inc.,
PO Box 2574,
West Lafayette, Indiana 47906

### Acknowledgements

DriveWire serial routines by Darren Atkinson from [here](https://sourceforge.net/p/toolshed/code/ci/default/tree/hdbdos/)

The console code uses Ciaran Anscomb's Dragon keyboard driver from [here](http://www.6809.org.uk/dragon/asm/keyboard.s)
